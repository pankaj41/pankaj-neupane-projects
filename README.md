
 **README**

**These Projects are created by Pankaj Neupane**

For detail information on the projects, readme file can be found inside each projects which takes into detail   
on the project scope and technology used. All the projects are created using Java 1.8 and NetBeans as IDE.  

 1. **Flooring Mastery**  
	 This is a Java console based projects which is created for a flooring company to add,  
 delete, update and read orders. This project uses .txt file to read and write orders.
 
 2. **AddressBook MVC with Database**  
	 Address Book MVC with Database is a full stack project which utilizies various technology 
	 in client side, uses spring framework in serverside for MVC, validation, security, jdbc 
	 and uses MySql as database. This web app allows users to add, update, read and delete address
	from the database. 
	This project is deployed online and can be viewed by clicking the link below. 
	https://swcguild-pankaz.rhcloud.com/AddressBookMVC/
	Role- Admin
	user name- test
	password-	password
	
	Role - user
	user name - test2
	password - password
	
	
	
 3. **The Venue- Content Management System**  
	The Venue is a full stack content management system developed for small business that host concerts.  
		The admin has ability to add and manage blogs and static pages. Blogs and static pages can be created  
		by admin or other authorized user where other user has limited functionality. Users can check out the 
		blogs and static pages withoug loggin in.   
	


