/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbook.test;

import com.tsg.addressbook.Dao.AddressBookDao;
import com.tsg.addressbook.Dao.SearchTerm;
import com.tsg.addressbook.dto.Address;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class AddressbookDaoTest {

    private AddressBookDao dao;
    private Address address1;
    private Address address2;
    private Address address3;

    public AddressbookDaoTest() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("addressBookDao", AddressBookDao.class);
        //can we use setjdbctemplet or inject here??
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("delete from address_list");

        address1 = new Address();
        address1.setfName("John");
        address1.setlName("Smith");
        address1.setAddress1("55280");
        address1.setAddress2("infiniteLoop drive");
        address1.setCity("mountain Hill");
        address1.setState("CA");
        address1.setZip("94598");

        address2 = new Address();
        address2.setfName("Kasey");
        address2.setlName("Smith");
        address2.setAddress1("55280");
        address2.setAddress2("Generics dr.");
        address2.setCity("Morgan Hills");
        address2.setState("CA");
        address2.setZip("94598");

        address3 = new Address();
        address3.setfName("Kasey");
        address3.setlName("Smith");
        address3.setAddress1("55280");
        address3.setAddress2("Generics dr.");
        address3.setCity("Morgan Hills");
        address3.setState("OH");
        address3.setZip("94598");

    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void addAddressTest(){
        //Arrange & act
        Address address = dao.addAddress(address1);
        
        //Assert
        assertNotEquals(address.getAddressIndex(), 0);
    }
    
    @Test
    public void getAddressByIndexTest(){
        
        //Arrange
        Address address = dao.addAddress(address2);
        //Act
        Address addressFromDb = dao.getAddressByIndex(address.getAddressIndex());
        //Assert
        assertEquals(address, addressFromDb);
        
        //Assert 2
        Address address2 = dao.getAddressByIndex(0);
        assertNull(address2);
        
    }
    @Test
    public void removeAddressTest() {
        dao.addAddress(address1);

        Address fromDB = dao.getAddressByIndex(address1.getAddressIndex());
        assertEquals(address1, fromDB);

        dao.removeAddress(address1.getAddressIndex());
        assertNull(dao.getAddressByIndex(address1.getAddressIndex()));;

    }


    @Test
    public void addUpdateAddressAddress() {
        //Arrange
        dao.addAddress(address1);
        address1.setCity("Akron");
        //Act
        dao.updateAddress(address1);        
        Address fromDb = dao.getAddressByIndex(address1.getAddressIndex());
        assertEquals(fromDb, address1);
    }

    @Test
    public void getAllAddressTest() {
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);
        List<Address> addressList = dao.getAllAddress();
        assertEquals(3, addressList.size());
    }

    @Test
    public void searchAddressTest() {
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);

        Map<SearchTerm, String> criteria = new HashMap<>();
        criteria.put(SearchTerm.LAST_NAME, "Smith");
//        int[] array = new int[100];
//        for (int i = 0; i < 100; i++) {
//            List<Address> aList= dao.searchAddress(criteria);
//            int a = aList.indexOf(address1);
//            array[i]=a;
//        }
        List<Address> aList = dao.searchAddress(criteria);

        assertEquals(3, aList.size());
        //it seems like there is no gurantee that this will be 0
        assertEquals(address1, aList.get(0));

        criteria.put(SearchTerm.STATE, "CA");
        aList = dao.searchAddress(criteria);
        assertEquals(2, aList.size());

        criteria.put(SearchTerm.FIRST_NAME, "Kasey");
        aList = dao.searchAddress(criteria);
        assertEquals(1, aList.size());

    }
}
