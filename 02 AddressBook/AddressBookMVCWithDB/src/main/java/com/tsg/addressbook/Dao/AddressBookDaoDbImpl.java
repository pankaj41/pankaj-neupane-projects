/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbook.Dao;

import com.tsg.addressbook.dto.Address;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoDbImpl implements AddressBookDao {

    //All sql statements are in the form of prepared statements
    //This will prevent sql injection attacks
    private static final String SQL_INSERT_ADDRESS
            = "INSERT INTO address_list(first_name,last_name,address1,address2,city,state,zip) values(?,?,?,?,?,?,?)";
    private static final String SQL_DELETE_ADDRESS
            = "delete from address_list where address_id=?";
    private static final String SQL_UPDATE_ADDRESS
            = "UPDATE `address_list` SET `first_name`=?,`last_name`=?,`address1`=?,`address2`=?,`city`=?,`state`=?,`zip`=? "
            + "WHERE address_id=?";
    private static final String SQL_SELECT_ADDRESS_BY_INDEX
            = "select * from address_list where address_id = ?";
    private static final String SQL_SELECT_ALL_ADDRESSES
            = "select * from address_list";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    //need transaction, if no transaction is present make one
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Address addAddress(Address address) {
        jdbcTemplate.update(SQL_INSERT_ADDRESS, address.getfName(),
                address.getlName(),
                address.getAddress1(),
                address.getAddress2(),
                address.getCity(),
                address.getState(),
                address.getZip());
        //gives us last inserted id  i guess, its a function that returns the last inserted id?????       
        address.setAddressIndex(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return address;
    }

    @Override
    public void removeAddress(int addressIndex) {
        jdbcTemplate.update(SQL_DELETE_ADDRESS, addressIndex);
    }

    @Override
    public void updateAddress(Address address) {
        jdbcTemplate.update(SQL_UPDATE_ADDRESS, address.getfName(), address.getlName(), address.getAddress1(), address.getAddress2(),
                address.getCity(), address.getState(), address.getZip(), address.getAddressIndex());
    }

    @Override
    public List<Address> getAllAddress() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ADDRESSES, new AddressMapper());
    }

    @Override
    public Address getAddressByIndex(int addressIndex) {

        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_ADDRESS_BY_INDEX, new AddressMapper(), addressIndex);// how is new addressMapper even related to the method maprow???
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Address> searchAddress(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return null;
        } else {
            StringBuilder sQuery = new StringBuilder("select * from address_list where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();
            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }
                sQuery.append(currentKey);
                sQuery.append(" =? ");
                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;

            }
            return jdbcTemplate.query(sQuery.toString(), new AddressMapper(), paramVals);
        }
    }
//this nexted class maps a row from the database into a Address object

    private static final class AddressMapper implements RowMapper<Address> {

        @Override
        public Address mapRow(ResultSet rs, int i) throws SQLException {
            Address address = new Address();
            address.setAddressIndex(rs.getInt("address_id"));
            address.setfName(rs.getString("first_name"));
            address.setlName(rs.getString("last_name"));
            address.setAddress1(rs.getString("address1"));
            address.setAddress2(rs.getString("address2"));
            address.setCity(rs.getString("city"));
            address.setState(rs.getString("state"));
            address.setZip(rs.getString("zip"));
            return address;
        }

    }

}
