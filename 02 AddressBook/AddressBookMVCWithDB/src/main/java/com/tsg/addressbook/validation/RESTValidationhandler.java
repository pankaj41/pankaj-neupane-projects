/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbook.validation;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
//We must mark this class as ControllerAdvice to let the SpringFramework know that the code in this
//class should be applied to our controller 
@ControllerAdvice  // what does that actually mean?????
public class RESTValidationhandler {

//    Mark this as an exception handler and specify which exception it handles 
//    Specify the HTTP Status code to return after this class processes the validation errors
//    Mark our return type with the @ReponseBody annotation so that the returned ValidationErrorContainer
//will be included in the response body
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorContainer processValidationErrors(MethodArgumentNotValidException e){
        //get the binding result and all field errors
        BindingResult result = e.getBindingResult(); //it seems like binding result it an object with all the errors in it
        List<FieldError> fieldErrors = result.getFieldErrors();
        
        //create a new ValidationError for each fieldError in the Binding Result
        ValidationErrorContainer errors = new ValidationErrorContainer();
        for (FieldError currentError : fieldErrors) {
            errors.addValidationError(currentError.getField(), currentError.getDefaultMessage());
        }
        
        return errors;
    }
    
    
    
    

}
