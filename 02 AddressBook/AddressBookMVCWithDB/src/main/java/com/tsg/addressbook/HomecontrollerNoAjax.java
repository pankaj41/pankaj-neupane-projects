/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbook;

import com.tsg.addressbook.Dao.AddressBookDao;
import com.tsg.addressbook.dto.Address;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */ 
@Controller
public class HomecontrollerNoAjax {
    private final AddressBookDao dao;
    
    //injecting the bean from the sprint-persistence.xml into the constructor of the class
    @Inject
    public HomecontrollerNoAjax(AddressBookDao dao) {
        this.dao=dao;
    }
    //this gets request from client to run something with value displayAddressBookNoAjax... then it list all the address from the memory and
    //deploys the displayAnddressBookNoAjax jsp...
    @RequestMapping(value="/displayAddressBookNoAjax", method = RequestMethod.GET)
    public String displayAddressBookNoAjax(Model model){
       List<Address> addressList = dao.getAllAddress();
       model.addAttribute("addressBookList", addressList);
       return "displayAddressBookNoAjax";
    }
    
    @RequestMapping(value="/displayNewAddressFormNoAjax",method = RequestMethod.GET)
    public String displayNewAddressFormNoAjax(Model model){
        Address address = new Address();
        model.addAttribute("address", address);
        return "newAddressFormNoAjax";
    }
    
    @RequestMapping(value="/addNewAddressNoAjax",method = RequestMethod.POST)
    public String addnewAddressNoAjax(@Valid @ModelAttribute("address")Address address, BindingResult  result){//this is request as we are not responding.. 
         if (result.hasErrors()) {
            return "newAddressFormNoAjax";
        }
//        String fName = req.getParameter("fName");
//        String lName = req.getParameter("lName");
//        String address1 = req.getParameter("address1");
//        String address2 = req.getParameter("address2");
//        String city = req.getParameter("city");
//        String state=req.getParameter("state");
//        String zip = req.getParameter("zip");
        
//        Address address = new Address();
//        address.setfName(fName);
//        address.setlName(lName);
//        address.setAddress1(address1);
//        address.setAddress2(address2);
//        address.setCity(city);
//        address.setState(state);
//        address.setZip(zip);
//        dao.addAddress(address);
 dao.addAddress(address);
        //return  "redirect:displayAddressBookNoAjax";
        return "redirect:displayAddressBookNoAjax";
        //why it cannot directly return displayAddressBookNoAjax... what is the use of redirect
    }
    @RequestMapping(value="/deleteAddressNoAjax",method = RequestMethod.GET)
    public String deleteAddressNoAjax(HttpServletRequest req){
        int addressIndex = Integer.parseInt(req.getParameter("addressIndex"));
        dao.removeAddress(addressIndex);
        return "redirect:displayAddressBookNoAjax";
    }
    
    @RequestMapping(value="/displayEditAddressFormNoAjax",method = RequestMethod.GET)
    public String displayEditAddressFormNoAjax(HttpServletRequest req,Model model){
        int addressIndex = Integer.parseInt(req.getParameter("addressIndex"));
//        !!!! address and dao(address is not the same object.. check the hashnumber) OR NOT
       Address address = dao.getAddressByIndex(addressIndex);
       model.addAttribute("address", address);
       return "editAddressFormNoAjax";
    }
    //!!!Bug... if index 0 is deleated and any other is edited.. 0 reapprears
    //question on editAddressFormNoAjax
    @RequestMapping(value="/editAddressNoAjax",method = RequestMethod.POST)
    public String editAddressnoAjax(@Valid @ModelAttribute("address")Address address, BindingResult  result){
        if (result.hasErrors()) {
            return "editAddressFormNoAjax";
        }
        //dao.updateAddress(address);
        return  "redirect:displayAddressBookNoAjax";
    }
    
   
    
}
