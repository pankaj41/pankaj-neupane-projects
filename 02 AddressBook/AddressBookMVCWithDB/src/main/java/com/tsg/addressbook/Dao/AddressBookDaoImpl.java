/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbook.Dao;

import com.tsg.addressbook.dto.Address;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoImpl implements AddressBookDao{
    private Map<Integer,Address> addressMap = new HashMap<>();
    private  int counter=0;
    @Override
    public Address addAddress(Address address) {
        address.setAddressIndex(counter);
        counter++;
        addressMap.put(address.getAddressIndex(), address);
        return address;
    }

    @Override
    public void removeAddress(int addressIndex) {
        addressMap.remove(addressIndex);
    }

    @Override
    public void updateAddress(Address address) {
        addressMap.put(address.getAddressIndex(), address);
    }

    @Override
    public List<Address> getAllAddress() {
        Collection col = addressMap.values();
        return new ArrayList(col);
    }

    @Override
    public Address getAddressByIndex(int addressIndex) {
        return addressMap.get(addressIndex);
    }

    @Override
    public List<Address> searchAddress(Map<SearchTerm, String> criteria) {
        String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
        String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
        String address2 = criteria.get(SearchTerm.ADDRESS2);
        String address1 = criteria.get(SearchTerm.ADDRESS1);
        String cityCriteria = criteria.get(SearchTerm.CITY);
        String stateCriteria= criteria.get(SearchTerm.STATE);
        String zipCriteria = criteria.get(SearchTerm.ZIP);
        
        Predicate<Address> firstNameMatch;
        Predicate<Address> lastNameMatch;
        Predicate<Address> houseNumberMatch;
        Predicate<Address> streetAddressMatch;
        Predicate<Address> cityMatch;
        Predicate<Address> stateMatch;
        Predicate<Address> zipMatch;
        
        Predicate<Address> truePredicate = c-> {return true;};
        
        firstNameMatch=(firstNameCriteria==null||firstNameCriteria.isEmpty())?truePredicate: c -> c.getfName().equalsIgnoreCase(firstNameCriteria);
         lastNameMatch=(lastNameCriteria==null||lastNameCriteria.isEmpty())?truePredicate: c -> c.getlName().equalsIgnoreCase(lastNameCriteria);
         houseNumberMatch=(address2==null||address2.isEmpty())?truePredicate: c -> c.getAddress1().equalsIgnoreCase(address2);
         streetAddressMatch=(address1==null||address1.isEmpty())?truePredicate: c-> c.getAddress2().equalsIgnoreCase(address1);
         cityMatch=(cityCriteria==null||cityCriteria.isEmpty())?truePredicate: c-> c.getCity().equalsIgnoreCase(cityCriteria);
         zipMatch=(zipCriteria==null||zipCriteria.isEmpty())?truePredicate:c -> c.getZip().equalsIgnoreCase(zipCriteria);
         stateMatch =(stateCriteria==null||stateCriteria.isEmpty())?truePredicate:c -> c.getState().equalsIgnoreCase(stateCriteria);
        return addressMap.values().stream()
                .filter(firstNameMatch.and(lastNameMatch).and(houseNumberMatch).and(streetAddressMatch)
                .and(cityMatch).and(zipMatch).and(stateMatch))
                .collect(Collectors.toList());
    }
    
}
