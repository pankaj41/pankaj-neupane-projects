<%-- 
    Document   : home
    Created on : Mar 28, 2016, 6:18:21 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>

        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/mainAjaxPage"  >AddressBook (Ajax)</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayAddressBookNoAjax">
                            Contact List (No Ajax)
                        </a>
                    </li>
                    <!--  - Logout link -->
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                    </li> 
                </ul>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h2>Address Book</h2>
                    <table id="addressTable" class="table table-hover">
                        <tr>                        
                            <th class="col-md-5"> Name</th>
                            <th class="col-md-3"> State</th>
                            <th class="col-md-2"> </th>
                            <th class="col-md-2"> </th>
                        </tr>
                        <tbody id="contentRows">

                        </tbody>
                    </table>
                </div>
                <!--add address list-->
                <div class="col-md-6">
                    <h2>Add new Address</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8">
                                <input type="text"
                                       class="form-control"
                                       id="add-first-name"
                                       placeholder="First Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                            <div class="col-md-8">
                                <input type="text"
                                       class="form-control"
                                       id="add-last-name"
                                       placeholder="Last Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-address1" class="col-md-4 control-label">Address1 :</label>
                            <div class="col-md-8">
                                <input type="text"
                                       class="form-control"
                                       id="add-address1"
                                       placeholder="Street Address, P.O box,company name,c/o">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-address2" class="col-md-4 control-label">Address2 :</label>
                            <div class="col-md-8">
                                <input type="text"
                                       class="form-control"
                                       id="add-address2"
                                       placeholder="Apartment,Suite,unit,Building, Floor,etc">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-city" class="col-md-4 control-label">City :</label>
                            <div class="col-md-8">
                                <input type="text"
                                       class="form-control"
                                       id="add-city"
                                       placeholder="City">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="add-state">State:</label>
                            <div class="col-md-8">
                                <input type="text"
                                       class="form-control"
                                       id="add-state"
                                       placeholder="State">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="add-zip">Zip:</label>
                            <div class="col-md-8">
                                <input type="number"
                                       class="form-control"
                                       id="add-zip"
                                       placeholder="Zip">
                            </div>
                        </div>
                        <div class="col-md-offset-4 col-md-4">
                            <button class="btn btn-default"  id="add-button">
                                Add Address
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!--        <div class="modal fade" id="detailsModal" tabindex="1" role="dialog" >//what does tabindex-1 does
                    <div class="modal-dialog">
                        <div class="modal-content">-->
        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                //added
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="detailsModalLabel">Address Details</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="address-id"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>First Name:</th>
                                <td id="address-firstName"></td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td id="address-lastName"></td>
                            </tr>
                            <tr>
                                <th>Address1:</th>
                                <td id="address-address1"></td>
                            </tr>
                            <tr>
                                <th>Address2:</th>
                                <td id="address-address2"></td>
                            </tr>
                            <tr>
                                <th>City:</th>
                                <td id="address-city"></td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td id="address-state"></td>
                            </tr>
                            <tr>
                                <th>Zip:</th>
                                <td id="address-zip"></td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>  
        <!--edit modal-->
        <div class="modal fade" id="editModal" tabindex="1" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">Close</button>
                        <h4 class="modal-title" id="detailsModalLabel">Edit Address</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="address-id"></h3>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-first-name" class="col-md-4 control-label" >
                                    First Name:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-first-name" placeholder="First Name">                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-last-name" class="col-md-4 control-label" >
                                    Last Name:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-last-name" placeholder="Last Name">                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-address1" class="col-md-4 control-label" >
                                    Address1:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-address1" placeholder="Address1">                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-address2" class="col-md-4 control-label" >
                                    Address2:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-address2" placeholder="Address2">                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-city" class="col-md-4 control-label" >
                                    City:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-city" placeholder="City">                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-state" class="col-md-4 control-label" >
                                    State:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-state" placeholder="State">                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-zip" class="col-md-4 control-label" >
                                    Zip:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-zip" placeholder="Zip">                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="button" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit Address</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" id="edit-address-id">
                                </div>
                            </div>
                        </form>
                        <div id="validationErrorsEdit" style="color:red" class="text-left col-md-offset-5" ></div>
                    </div>
                </div>
            </div>

        </div>
        <div id="validationErrors" style="color:red" class="col-md-offset-8"></div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/mockData.js"></script>
        <script src="${pageContext.request.contextPath}/js/addressList.js"></script>
    </body>
</html>
