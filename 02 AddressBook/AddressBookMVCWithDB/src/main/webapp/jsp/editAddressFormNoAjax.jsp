<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AddressBook Form</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>

        </div>
        <div class="container">
            <h1>Edit Address Book Form</h1>
            <hr/>
            <a href="displayAddressBookNoAjax">Address Book(No Ajax)</a>
            <hr/>
            //so is it updating the model object and sending it back.. if yes how??
            <sf:form action="editAddressNoAjax" method="Post" class="form-horizontal" role="form" modelAttribute="address">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="add-first-name">First Name:</label>
                    <div class="col-md-6">
                        <!--path is the property of model object????-->
                        <sf:input type="text" class="form-control" id="add-first-name" path="fName" placeholder="First Name"/>
                        <sf:errors path="fName" cssClass="text-danger"></sf:errors>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="add-last-name">Last Name:</label>
                    <div class="col-md-6">
                        <sf:input type="text" class="form-control" id="add-last-name" path="lName" placeholder="Last Name"/>
                        <sf:errors path="lName" cssClass="text-danger"></sf:errors>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="add-address1">Address1:</label>
                    <div class="col-md-6">
                        <sf:input type="text" class="form-control" id="add-address1" path="address1" placeholder="Street Address, P.O box,company name,c/o"/>
                        <sf:errors path="address1" cssClass="text-danger"></sf:errors>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="add-address2">Address 2:</label>
                    <div class="col-md-6">
                        <sf:input type="text" class="form-control" id="add-address2" path="address2" placeholder="Apartment,Suite,unit,Building, Floor,etc"/>
                        <sf:errors path="address2" cssClass="text-danger"></sf:errors>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="add-city">City:</label>
                    <div class="col-md-6">
                        <sf:input type="text" class="form-control" id="add-city" path="city" placeholder="City"/>
                        <sf:errors path="city" cssClass="text-danger"></sf:errors>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="add-state">State:</label>
                    <div class="col-md-6">
                        <sf:input type="text" class="form-control" id="add-state" path="state" placeholder="State"/>
                        <sf:errors path="state" cssClass="text-danger"></sf:errors>
                        <sf:hidden path="addressIndex"></sf:hidden>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="add-zip">Zip code:</label>
                    <div class="col-md-6">
                        <sf:input type="text" class="form-control" id="add-zip" path="zip" placeholder="Zip Code"/>
                        <sf:errors path="zip" cssClass="text-danger"></sf:errors>
                    </div>                    
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-6">
                        <button type="submit" id="add-button" class="btn btn-default">Edit</button>
                    </div>                    
                </div> 
                


            </sf:form>
        </div>
        

    </body>
</html>
