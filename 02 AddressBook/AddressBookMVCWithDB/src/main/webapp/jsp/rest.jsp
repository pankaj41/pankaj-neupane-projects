<%-- 
    Document   : rest
    Created on : Apr 4, 2016, 8:22:48 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <h1>Address Application</h1>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <div id="addressTableDiv">
                        <h2>My Address</h2>
                        <table id="addressTable" class="table table-hover">
                            <tr>
                                <th class="col-md-4">Name</th>
                                <th class="col-md-3">State</th>
                                <th class="col-md-1.5"></th>
                                <th class="col-md-1.5"></th>
                            </tr>
                            <tbody id="contentRows">

                            </tbody>
                        </table>                        
                    </div>                    
                </div>
                <div class="col-md-6">
                    <div id="editFormDiv" style="display: none">
                        <h2>Edit Address</h2>
                        <form class="form-horizontal" role="form"> 
                            <div class="form-group">
                                <label for="edit-first-name" class="col-md-4 control-label">First Name:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="edit-first-name"
                                           placeholder="First Name">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="edit-last-name" class="col-md-4 control-label">Last Name:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="edit-last-name"
                                           placeholder="Last Name">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="edit-address1" class="col-md-4 control-label">Address1:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="edit-address1"
                                           placeholder="Address1">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="edit-address2" class="col-md-4 control-label">Address2:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="edit-address2"
                                           placeholder="Address2">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="edit-city" class="col-md-4 control-label">City:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="edit-city"
                                           placeholder="City">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="edit-state" class="col-md-4 control-label">State:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="edit-state"
                                           placeholder="State">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="edit-zip" class="col-md-4 control-label">Zip:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="edit-zip"
                                           placeholder="Zip">
                                </div>                                    
                            </div>

                            <div class="form-group ">
                                <div class="col-md-offset-4">
                                    <input type="hidden" id="edit-address-id">
                                    <button class="btn btn-default"   onclick="hideEditForm()">
                                        Cancel
                                    </button>
                                    <button class="btn btn-default"   id="edit-button">
                                        Edit Address
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div id="addFormDiv">
                        <h2>Add Address</h2>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-first-name" class="control-label col-md-4">First Name:</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="add-first-name" type="text" placeholder="First Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="add-last-name"
                                           placeholder="Last Name">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="add-address1" class="col-md-4 control-label">Address1:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="add-address1"
                                           placeholder="Address1">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="add-address2" class="col-md-4 control-label">Address2:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="add-address2"
                                           placeholder="Address2">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="add-city" class="col-md-4 control-label">City:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="add-city"
                                           placeholder="City">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="add-state" class="col-md-4 control-label">State:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="add-state"
                                           placeholder="State">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="add-zip" class="col-md-4 control-label">Zip:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="add-zip"
                                           placeholder="Zip">
                                </div>                                    
                            </div>

                            <div class="col-md-offset-4 col-md-4">
                                <button class="btn btn-default"  id="add-button">
                                    Add Address
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="validationErrors" style="color:red" class="col-md-offset-8"></div>
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/restAddressList.js"></script>
    </body>
</html>
