<%-- 
    Document   : home
    Created on : Mar 28, 2016, 6:18:21 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/mainAjaxPage"  >AddressBook (Ajax)</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/displayAddressBookNoAjax">
                            Contact List (No Ajax)
                        </a>
                    </li>
                    <!--  - Logout link -->
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                    </li> 
                </ul>
            </div>
        </div>
        <div class="container">
            <h1>Address from Address book</h1>  
            <!--Personalized Welcome Message-->
            Hello <sec:authentication property="principal.username" />!<br/>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <a href="displayNewAddressFormNoAjax">Add an address</a>
            </sec:authorize>
            <hr/>
        </div>
        <div class="container">

            <c:forEach var="address" items="${addressBookList}">
                <s:url value="deleteAddressNoAjax" var="deleteAddress_url">
                    <s:param name="addressIndex" value="${address.addressIndex}" />                   
                </s:url>
                <s:url value="displayEditAddressFormNoAjax" var="editAddress_url">
                    <s:param name="addressIndex" value="${address.addressIndex}" />                   
                </s:url>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Name:
                    </div>
                    <div class="col-md-8 text-left">
                        ${address.fName} ${address.lName}
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            |<a href="${deleteAddress_url}">Delete</a> |
                            <a href="${editAddress_url}">Edit</a>
                        </sec:authorize>
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address:
                    </div>
                    <div class="col-md-8 text-left">
                        ${address.address1} ${address.address2}<br/>
                        ${address.city}, ${address.state} ${address.zip}<br/>
                        ${address.addressIndex}
                    </div>
                </div>              

                <hr/>
            </c:forEach>


        </div>
    </body>
</html>
