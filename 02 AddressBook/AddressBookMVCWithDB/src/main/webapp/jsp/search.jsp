<%-- 
    Document   : search
    Created on : Mar 28, 2016, 6:43:12 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/mainAjaxPage"  >AddressBook (Ajax)</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/search" >Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayAddressBookNoAjax">
                            Contact List (No Ajax)
                        </a>
                    </li>
                    <!--  - Logout link -->
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                    </li> 
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="addressTableDiv">
                        <h2>My Address</h2>
                        <table id="addressTable" class="table table-hover">
                            <tr>
                                <th class="col-md-4">Name</th>
                                <th class="col-md-3">State</th>
                                <th class="col-md-1.5"></th>
                                <th class="col-md-1.5"></th>
                            </tr>
                            <tbody id="contentRows">

                            </tbody>
                        </table>                        
                    </div>                    
                </div>
                <div class="col-md-6">
                    <div id="searchFormDiv">
                        <h2 class="col-md-offset-4">Search Address</h2>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="search-first-name" class="control-label col-md-4">First Name:</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="search-first-name" type="text" placeholder="First Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="search-last-name" class="col-md-4 control-label">Last Name:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="search-last-name"
                                           placeholder="Last Name">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="search-address1" class="col-md-4 control-label">Address1:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="search-address1"
                                           placeholder="Address1">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="search-address2" class="col-md-4 control-label">Address2:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="search-address2"
                                           placeholder="Address2">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="search-city" class="col-md-4 control-label">City:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="search-city"
                                           placeholder="City">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="search-state" class="col-md-4 control-label">State:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="search-state"
                                           placeholder="State">
                                </div>                                    
                            </div>
                            <div class="form-group">
                                <label for="search-zip" class="col-md-4 control-label">Zip:</label>
                                <div class="col-md-8">
                                    <input type="text" 
                                           class="form-control"
                                           id="search-zip"
                                           placeholder="Zip">
                                </div>                                    
                            </div>

                            <div class="col-md-offset-4 col-md-4">
                                <button class="btn btn-default"  id="search-button">
                                    Search Address
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/addressList.js"></script>
        <script src="${pageContext.request.contextPath}/js/addressSearchList.js"></script>
    </body>
</html>
