$(document).ready(function (){
    
        $('#search-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "search/addresses",
            //adding request body and making it json
            data: JSON.stringify({
                fName: $('#search-first-name').val(),
                lName: $('#search-last-name').val(),
                address1: $('#search-address1').val(),
                address2: $('#search-address2').val(),
                city: $('#search-city').val(),
                state: $('#search-state').val(),
                zip: $('#search-zip').val()
            }), //creating headers
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#search-first-name').val("");
            $('#search-last-name').val("");
            $('#search-address1').val("");
            $('#search-address2').val("");
            $('#search-city').val("");
            $('#search-state').val("");
            $('#search-zip').val("");
            fillAddressTable(data,status);
           
        });
    });
});

function fillAddressTable(data, status){
    clearAddressTable();
 var aTable = $('#contentRows');
      $.each(data, function (index, address) { //what is the us of address here?
            aTable.append($('<tr>')
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({//when you hit the a link data-toggle moodal with the information....
                                        'data-address-id': address.addressIndex,
                                        'data-toggle': 'modal',
                                        'data-target': '#detailsModal'
                                    })
                                    .text(address.fName + " " + address.lName)
                                    )//end of <a> append
                            )//end of <td> append
                    .append($('<td>').text(address.state))
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({// this link will activate data toggle edit window
                                        'data-address-id': address.addressIndex,
                                        'data-toggle': 'modal',
                                        'data-target': '#editModal'
                                    })
                                    .text('Edit')
                                    )//end the <a> tag
                            )//ends the <td> tag for edit
                    .append($('<td>')
                            .append($('<a>').attr({'onclick': 'deleteAddress(' + address.addressIndex + ')'})
                                    .text('Delete'))//end of <a> append
                            )//end of <td>append                
                    );//end of <tr> append

        });
}

function clearAddressTable() {
    $('#contentRows').empty();
}
