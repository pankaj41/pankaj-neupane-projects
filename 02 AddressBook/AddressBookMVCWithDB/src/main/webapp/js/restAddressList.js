$(document).ready(function () {
    loadAddress();
    addAddressToDataBase();
    editAddressToDataBase();
});


function addAddressToDataBase() {
    $('#add-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "address/",
            //adding request body and making it json
            data: JSON.stringify({
                fName: $('#add-first-name').val(),
                lName: $('#add-last-name').val(), 
                address1: $('#add-address1').val(),
                address2: $('#add-address2').val(),
                city: $('#add-city').val(),
                state: $('#add-state').val(),
                zip: $('#add-zip').val()
            }), //creating headers
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-first-name').val("");
            $('#add-last-name').val("");
            $('#add-address1').val("");
            $('#add-address2').val("");
            $('#add-city').val("");
            $('#add-state').val("");
            $('#add-zip').val("");
            $("#validationErrors").empty();
            loadAddress();
        }).error(function (data, status) {
            //clean the div even if there is error.. 
            $("#validationErrors").empty();
            //extracting data.responseJSON.fieldErrors which is extracting fieldErrors from data in form of json and inside function we are just calling it valididationError
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');

                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });
}

function editAddressToDataBase() {


    $("#edit-button").click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'PUT',
            url: "address/" + $('#edit-address-id').val(),
            data: JSON.stringify({
                fName: $('#edit-first-name').val(),
                lName: $('#edit-last-name').val(), //is this variable lName same as the dto variable for last name?????
                address1: $('#edit-address1').val(),
                address2: $('#edit-address2').val(),
                city: $('#edit-city').val(),
                state: $('#edit-state').val(),
                zip: $('#edit-zip').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }, 'dataType': 'json'
        }).success(function (data, status) {
            hideEditForm();
            loadAddress();
            $("#validationErrors").empty();
        }).error(function (data, status) {
            //clean the div even if there is error.. 
            $("#validationErrors").empty();
            //extracting data.responseJSON.fieldErrors which is extracting fieldErrors from data in form of json and inside function we are just calling it valididationError
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });
}
function hideEditForm() {
    $('#edit-first-name').val("");
    $('#edit-last-name').val("");//is this variable lName same as the dto variable for last name?????
    $('#edit-address1').val("");
    $('#edit-address2').val("");
    $('#edit-city').val("");
    $('#edit-state').val("");
    $('#edit-zip').val("");
    $('#editFormDiv').hide();
    $('#addFormDiv').show();
}

function loadAddress() {
    clearAddressTable();
    var contentRows = $('#contentRows');
    $.ajax({
        type: 'GET',
        url: "/addresses"
    }).success(function (data, status) {
        $.each(data, function (index, address) {
            var name = address.fName + " " + address.lName;
            var state = address.state;
            var index = address.addressIndex;
            var row = '<tr>';
            row += '<td>' + name + '</td>';
            row += '<td>' + state + '</td>';
            row += '<td><a onclick="showEditForm(' + index + ')">Edit</a></td>';
            row += '<td><a onclick="deleteContactForm(' + index + ')">Delete</a></td>';
            row += '</tr>';
            contentRows.append(row);
        });
    });


}
function deleteContactForm(addressIndex) {
    $.ajax({
        type: 'DELETE',
        url: "address/" + addressIndex
    }).success(function () {
        loadAddress();
    });

}
function clearAddressTable() {
    $('#contentRows').empty();
}

function showEditForm(addressIndex) {
    //get addressbyindex
    $.ajax({
        url: "address/" + addressIndex
    }).success(function (address, status) {
        $('#edit-first-name').val(address.fName);
        $('#edit-last-name').val(address.lName);
        $('#edit-address1').val(address.address1);
        $('#edit-address2').val(address.address2);
        $('#edit-city').val(address.city);
        $('#edit-state').val(address.state);
        $('#edit-zip').val(address.zip);
        $('#edit-address-id').val(address.addressIndex);
        $('#editFormDiv').show();
        $('#addFormDiv').hide();
    });

}


