/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//document ready function
$(document).ready(function () {
    loadAddress();
    addAddressToDataBase();
    editAdddressToDataBase();
});

function addAddressToDataBase() {
    $('#add-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: "address/",
            //adding request body and making it json
            data: JSON.stringify({
                fName: $('#add-first-name').val(),
                lName: $('#add-last-name').val(),
                address1: $('#add-address1').val(),
                address2: $('#add-address2').val(),
                city: $('#add-city').val(),
                state: $('#add-state').val(),
                zip: $('#add-zip').val()
            }), //creating headers
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-first-name').val("");
            $('#add-last-name').val("");
            $('#add-address1').val("");
            $('#add-address2').val("");
            $('#add-city').val("");
            $('#add-state').val("");
            $('#add-zip').val("");
            loadAddress();
            $('#validationErrors').empty();
        }).error(function (data, status) {
            $('#validationErrors').empty();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));

            });
        });
    });
}

function editAdddressToDataBase() {
    $('#edit-button').click(function () {
        $.ajax({
            type: 'PUT',
            url: "address/" + $('#edit-address-id').val(),
            data: JSON.stringify({
                fName: $('#edit-first-name').val(),
                lName: $('#edit-last-name').val(),
                address1: $('#edit-address1').val(),
                address2: $('#edit-address2').val(),
                city: $('#edit-city').val(),
                state: $('#edit-state').val(),
                zip: $('#edit-zip').val()
            }), headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }, 'dataType': 'json'
        }).success(function () {
            loadAddress();
            $('#edit-button').attr('data-dismiss', 'modal');
            $('#validationErrorsEdit').empty();
        }).error(function (data, status) {
            $('#validationErrorsEdit').empty();
            $('#edit-button').attr('data-dismiss', '');
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrorsEdit');
                errorDiv.append(validationError.message).append($('<br>'));

            });
        });
    });
}

//load address into the summary table
function loadAddress() {
    //clear the previous list
    clearAddressTable();//clear table everytime the load address is run 
    //grab the tbody emement that will hold the new list of contacts
    var aTable = $('#contentRows');
    $.ajax({
        url: "addresses/"
    }).success(function (data, status) {
        $.each(data, function (index, address) { //what is the us of address here?
            aTable.append($('<tr>')
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({//when you hit the a link data-toggle moodal with the information....
                                        'data-address-id': address.addressIndex,
                                        'data-toggle': 'modal',
                                        'data-target': '#detailsModal'
                                    })
                                    .text(address.fName + " " + address.lName)
                                    )//end of <a> append
                            )//end of <td> append
                    .append($('<td>').text(address.state))
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({// this link will activate data toggle edit window
                                        'data-address-id': address.addressIndex,
                                        'data-toggle': 'modal',
                                        'data-target': '#editModal'
                                    })
                                    .text('Edit')
                                    )//end the <a> tag
                            )//ends the <td> tag for edit
                    .append($('<td>')
                            .append($('<a>').attr({'onclick': 'deleteAddress(' + address.addressIndex + ')'})
                                    .text('Delete'))//end of <a> append
                            )//end of <td>append                
                    );//end of <tr> append

        });
    });
    //Iterate through each of the Json objects in the test address data
    //and render to the summary table

}

function clearAddressTable() {
    $('#contentRows').empty();
}

function deleteAddress(addressIndex) {
    $.ajax({
        url: "address/" + addressIndex,
        type: 'DELETE'
    }).success(function () {
        loadAddress();
    });
}

//when hit link activates data-target="#detailsModal
$('#detailsModal').on('show.bs.modal', function (event) { //when you click the link with id detailsModal it will fire up the show modal function of bootstrap
    // Get the element that triggered this event - in our case it is an address
    // name link in the summary table. This link has an attribute that contains
    // the contactId for the given address. We'll use that to retrieve the
    // address's details.
    var element = $(event.relatedTarget); //getting the html element
    //grab contact id
    var addressId = element.data('address-id');  //this is getting value of address-id from the the element
    var modal = $(this);
    $.ajax({
        url: "address/" + addressId
    }).success(function (address) {
        modal.find('#address-id').text(address.addressIndex);
        modal.find('#address-firstName').text(address.fName);
        modal.find('#address-lastName').text(address.lName);
        modal.find('#address-address1').text(address.address1);
        modal.find('#address-address2').text(address.address2);
        modal.find('#address-city').text(address.city);
        modal.find('#address-state').text(address.state);
        modal.find('#address-zip').text(address.zip);
    });
});
//this will show modal when the link is clicked
$('#editModal').on('show.bs.modal', function (event) {
    // Get the element that triggered this event - in our case it is a contact
    // name link in the summary table. This link has an attribute that contains
    // the contactId for the given contact. We'll use that to retrieve the
    // contact's details.
    var element = $(event.relatedTarget);
    // Grab the contact id from the html element
    var addressId = element.data('address-id');//gives the addressindex
    // PLACEHOLDER: Eventually we'll make an ajax call to the server to get the
    // details for this contact but for now we'll load the dummy
    // data
    var modal = $(this);
    $.ajax({
        url: "address/" + addressId
    }).success(function (address) {
        modal.find('#edit-address-id').val(address.addressIndex);
        modal.find('#edit-first-name').val(address.fName);
        modal.find('#edit-last-name').val(address.lName);
        modal.find('#edit-address1').val(address.address1);
        modal.find('#edit-address2').val(address.address2);
        modal.find('#edit-city').val(address.city);
        modal.find('#edit-state').val(address.state);
        modal.find('#edit-zip').val(address.zip);
        
    });

});


