**AddressBookMVCWIthDB**


This is an example of full stack web project which uses variety of technology to demonstrate   
the proficiency in the following fields.  

***1) Java as Back-End or server side language***  

 - Java is primarily used as the main server side language. Inheritance , polymorphism are   
 some of the core java concept used to build the back end function. 
 -  One of the use of Spring Frame work in this project is the dependency injection through
   xml which is used to update the instance of DAO as well as used for unit testing of DAO. 
 - Again Spring framework is used to organize the project in MVC pattern.  
  
 

***2)Non-Web services client side  and server side(No Ajax)***  

 - To demonstrate a regular(non-web service) functionality JSTL tags are used in jsp so that   
 only required information are sent out to the client side rather than sending out everything.   
 - Form validation is done on the server side using spring form tags and other spring annotations  
 - HTML in jsp with bootstrap with minimum use of Javascript is on the Client side.  
 
 ***3)Restful Web-Services with Ajax***   
 

 - Primarily JSON is used to serialize the data to transfer between client side and server side.  
 - Still JSP is used where HTML code is accompanied by bootstrap and CSS  along with Javascript.   
 - JQuery is heavily utilized along with Ajax which serialize the JSON data on the client side.   
 - Form is validated using spring validation as well.


***4)MySQL  database***

 - MySql is used as the database to store data.   
 - Spring JDBC template is used to connect DAO with the database.   
 - Currently innerclass in the DAO is used are ORM.   
 - Prepared statements are used so that sql injection can be prevented.   
 
 ***5)Spring Security***
 

 - Spring security is used to log in and authenticate the user.   
 - Currently password is stored in the database and spring looks it up and assign the role as   
 described in the database. 
 - To restrict unauthorized users, end points are locked in spring-security.xml configuration.  
 - Conditional Rendering of Page elements are also done using the spring security tag lib in jsp   
 to lock features for certain users as well as display logged in user's name.
 
 