/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.Order;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class OrderDAOTest {

    public OrderDAOTest() {
    }

    OrderDAO dao;

    Order o1;
    Order o2;
    Order o3;

    @Before
    public void setUp() {

        dao = new OrderDAO();

        o1 = new Order();
        o1.setOrderNumber(1);
        o1.setDate("03082016");
        o1.setCustomerName("Michael Knight");
        o1.setState("OH");
        o1.setProductType("Carpet");
        o1.setArea(300);

        o2 = new Order();
        o2.setOrderNumber(2);
        o2.setDate("03092016");
        o2.setCustomerName("Julia Roberts");
        o2.setState("IL");
        o2.setProductType("Laminate");
        o2.setArea(700);

        o3 = new Order();
        o3.setOrderNumber(3);
        o3.setDate("03102016");
        o3.setCustomerName("Forrest Gump");
        o3.setState("IN");
        o3.setProductType("Wood");
        o3.setArea(800);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of loadFileForGivenDate method, of class OrderDAO.
     */
    @Test
    public void addOrdersAndGetOrderByOrderNumberTest() {

        //arrange
        dao.addOrder(o1);

        //act
        Order orderReturned = dao.getOrderByOrderNumber(o1.getOrderNumber());

        //assert
        assertEquals(o1, orderReturned);

    }

    @Test
    public void removeOrderTest() {
        //arrange

        dao.addOrder(o2);
        dao.addOrder(o1);

        //act
        Order orderReturned = dao.removeOrder(o1.getOrderNumber());
        Order orderByNumber = dao.getOrderByOrderNumber(o1.getOrderNumber());

        //assert
        assertEquals(o1, orderReturned);//remove order gives the same object which was created.
        assertNull(orderByNumber);//checks if the object is null i.e. if there is no object by that order number
    }

    @Test
    public void updateOrderTest() {
        ////arrange
        //area of o1 is 300 to begin with
        Order orderBeforeEdit = new Order();

        orderBeforeEdit = dao.addOrder(o1);

        //Act
        //changing field
        orderBeforeEdit.setTotal(0);
        orderBeforeEdit.setArea(500);
        Order orderUpdate = dao.updateOrder(orderBeforeEdit, orderBeforeEdit.getOrderNumber());
        Order orderByOrderNumber = dao.getOrderByOrderNumber(o1.getOrderNumber());
        //assert
        //assertEquals(o1, orderUpdate);
        assertEquals(orderBeforeEdit, orderUpdate);// this shows it is the same object
        assertNotEquals(300, orderUpdate.getArea(), 0.0001);//this shows it is not same as the initial value
        assertEquals(o1.getArea(), orderByOrderNumber.getArea(), 0.0001);//area of object updated and object returned from orderByorder number is same.
    }

    @Test
    public void saveLoadOrderTest() {
        //arrange 
        dao.addOrder(o1);
        dao.addOrder(o2);
        dao.addOrder(o3);
        

        OrderDAO readDao = new OrderDAO();

        try {
            dao.writeFile(o1.getDate());
            dao.writeFile(o2.getDate());
            dao.writeFile(o3.getDate());
        } catch (IOException ex) {
            Logger.getLogger(OrderDAOTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }

        try {
            readDao.loadFile(o1.getDate());
            readDao.loadFile(o2.getDate());
            readDao.loadFile(o3.getDate());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDAOTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }

        Order read1 = readDao.getOrderByOrderNumber(o1.getOrderNumber());
        Order read2 = readDao.getOrderByOrderNumber(o2.getOrderNumber());
        Order read3 = readDao.getOrderByOrderNumber(o3.getOrderNumber());

        assertEquals(o1, read1);//loads file for only that date
        assertEquals(o2, read2);
        assertEquals(o3, read3);

    }

    @Test
    public void readWriteConfigFileTest() {

        o1.setOrderNumber(55);

        dao.orderMap.put(o1.getOrderNumber(), o1);

        try {
            dao.writeMaxOrderNumber();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDAOTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }

        OrderDAO readDao = new OrderDAO();

        try {
            readDao.loadConfigFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        int orderFromReadDAO = readDao.getOrderNumber();
        assertEquals(o1.getOrderNumber(), orderFromReadDAO);

    }

    @Test
    public void writeFileForAllDatesInHashmapAndLoadFileForGivenDateTest() {

        dao.orderMap.put(o1.getOrderNumber(), o1);//date is 03082016
        dao.orderMap.put(o2.getOrderNumber(), o2);//date is 03092016
        dao.orderMap.put(o3.getOrderNumber(), o3);//date is 03092016
        Order order1 = new Order();
        Order order2 = new Order();
        try {
            dao.writeFileForAllDatesInHashMap();
        } catch (IOException ex) {
            Logger.getLogger(OrderDAOTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        OrderDAO readDao = new OrderDAO();

        try {
            readDao.loadFileForGivenDate(o1.getDate());
            order1 = readDao.getOrderByOrderNumber(o1.getOrderNumber());//loaded only for that date
            order2 = readDao.getOrderByOrderNumber(o2.getOrderNumber());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(o1, order1);
        assertNull(order2);

        try {
            readDao.loadFileForGivenDate(o2.getDate());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Order order2_ =readDao.getOrderByOrderNumber(o2.getOrderNumber());
        assertEquals(o2, order2_);

    }

}
