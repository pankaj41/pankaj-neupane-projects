/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.State;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StateDAOTest {
    StateDAO dao;
    
    public StateDAOTest() {
    }
    
    @Before
    public void setUp() {
        dao = new StateDAO();
    }
    
    @After
    public void tearDown() {
    }
     @Test
     public void loadStateTest(){
        try {
            //Arrange & Act
            dao.loadStateTax();
            ArrayList<State> stateArrayList= dao.stateTaxArrayList;
            //Assert equals
            assertEquals(stateArrayList.size() , 4);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StateDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
    
    @Test
    public void statenameTest(){
        try {
            //Arrange
            dao.loadStateTax();
            //Act
            ArrayList<String> stateName = dao.stateName();
            //Assert
            assertEquals(stateName.size(), 4);
            assertEquals(stateName.get(0), "OH");
            assertEquals(stateName.get(3), "PA");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StateDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void getStateTaxTest(){
        try {
            //Arrange
            dao.loadStateTax();
            ArrayList<String> stateName = dao.stateName();
            //Act
           double stateTax0  = dao.getStateTax(stateName.get(0));
           
           //Assert
            assertEquals(stateTax0, 6.25,0.0001);
           
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StateDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
}
