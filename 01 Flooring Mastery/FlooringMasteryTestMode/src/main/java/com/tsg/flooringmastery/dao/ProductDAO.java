/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ProductDAO {
    String DELIMITER = ",";
    ArrayList<Product> productList = new ArrayList<>();
    ArrayList<String> productNames;
    public void loadProductInfo() throws FileNotFoundException{
        Scanner sc = new Scanner(new BufferedReader(new FileReader("Product.txt")));
        String currentLine;
        String[] currentTokens;
        while (sc.hasNext()) {
            currentLine = sc.nextLine();
            currentTokens = currentLine.split(DELIMITER);
            
            Product product = new Product();
            product.setProductdType(currentTokens[0]);
            product.setCostPSF(Double.parseDouble(currentTokens[1]));
            product.setLaborPSF(Double.parseDouble(currentTokens[2]));
            productList.add(product);
        }
        sc.close();
    }
    
    public ArrayList<String> getProductTypeList(){
        productNames = new ArrayList<>();
        for (Product product : productList) {
            
            productNames.add(product.getProductdType());
            
        }
        return productNames;
    }
    public Product getproduct(String product){
        Product productObj= new Product();
        for (Product product1 : productList) {
            if (product1.getProductdType().equalsIgnoreCase(product)) {
                productObj=product1;
            }
        }
        return productObj;
    }
    
}
