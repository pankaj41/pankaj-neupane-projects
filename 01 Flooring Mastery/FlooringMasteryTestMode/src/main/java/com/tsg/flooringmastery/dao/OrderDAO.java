/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class OrderDAO {

    Order order = new Order();
    HashMap<Integer, Order> orderMap = new HashMap<>();
    private String dateString = new SimpleDateFormat("MMddyyyy").format(new Date());
    private int orderNumber;
    private int maxNumber;
    private String FILE_NAME = "Orders_" + dateString + ".txt";
    private static final String DELIMITER = ",";

    public String mode ="Production";

    public void loadFileForGivenDate(String date) throws FileNotFoundException {
        Collection<Order> orderMapValues = orderMap.values();
        Set<String> datesSet = new HashSet<>();
        for (Order order : orderMapValues) {
            datesSet.add(order.getDate());
        }

        if (!(datesSet.contains(date))) {//if dateSet doesn't conatin the date load file
            loadFile(date);

        }

    }

    public void writeFileForAllDatesInHashMap() throws IOException {
        if (mode.equalsIgnoreCase("production")) {

            Collection<Order> orderMapValues = orderMap.values();
            //creating set of dates in the hashMap
            Set<String> datesSet = new HashSet<>();
            for (Order order : orderMapValues) {
                datesSet.add(order.getDate());
            }
            for (String date : datesSet) {
                writeFile(date);
            }
            writeMaxOrderNumber();

        }
    }

    public void writeFile(String date) throws IOException {
        String fileName = "Orders_" + date + ".txt";
        PrintWriter writer = new PrintWriter(new FileWriter(fileName));

        Collection<Order> coll = orderMap.values();
        for (Order order : coll) {
            if (order.getDate().equals(date)) {
                writer.println(order.getOrderNumber() + DELIMITER
                        + order.getCustomerName() + DELIMITER
                        + order.getState() + DELIMITER
                        + order.getTaxRate() + DELIMITER
                        + order.getProductType() + DELIMITER
                        + order.getArea() + DELIMITER
                        + order.getCostPerSqft() + DELIMITER
                        + order.getLaborCostPerSqFt() + DELIMITER
                        + order.getMaterialCost() + DELIMITER
                        + order.getLaborCost() + DELIMITER
                        + order.getTax() + DELIMITER
                        + order.getTotal() + DELIMITER
                        + order.getDate());

            }
            writer.flush();

        }
        writer.close();
    }

    public Order addOrder(Order order) {
        orderNumber++;
        order.setOrderNumber(orderNumber);
        order.setDate(dateString);

        orderMap.put(order.getOrderNumber(), order);
        return order;
    }

    public void loadTodaysFile() throws IOException {
        //create new file for today if it already doesn't exist
        File file = new File("Orders_" + dateString + ".txt");
        //load file if it exist
        if (file.exists()) {
            loadFileForGivenDate(dateString);
            //else create file   
        } else {
            File newFile = new File("Orders_" + dateString + ".txt");
            newFile.createNewFile();
            System.out.println("File doens't exist so new file is created.");//TODO delete
        }
    }

    public Order updateOrder(Order order, int orderNumber) {
        return orderMap.put(orderNumber, order);

    }

    public Order getOrderByOrderNumber(int orderNumber) {
        Order order = orderMap.get(orderNumber);
        return order;
    }

    public Order removeOrder(int orderNumber) {
        return orderMap.remove(orderNumber);
        //return removedOrder;
    }

    public void loadFile(String date) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("Orders_" + date + ".txt")));
        String currentLine;
        String[] currentToken;
        while (sc.hasNext()) {
            currentLine = sc.nextLine();
            currentToken = currentLine.split(DELIMITER);

            Order order = new Order();
            order.setOrderNumber(Integer.parseInt(currentToken[0]));
            order.setCustomerName(currentToken[1]);
            order.setState(currentToken[2]);
            order.setTaxRate(Double.parseDouble(currentToken[3]));
            order.setProductType(currentToken[4]);
            order.setArea(Double.parseDouble(currentToken[5]));
            order.setCostPerSqft(Double.parseDouble(currentToken[6]));
            order.setLaborCostPerSqFt(Double.parseDouble(currentToken[7]));
            order.setMaterialCost(Double.parseDouble(currentToken[8]));
            order.setLaborCost(Double.parseDouble(currentToken[9]));
            order.setTax(Double.parseDouble(currentToken[10]));
            order.setTotal(Double.parseDouble(currentToken[11]));
            order.setDate(currentToken[12]);

            orderMap.put(order.getOrderNumber(), order);

        }
        sc.close();

    }

    public void loadConfigFile() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("config.txt")));
        String currentLine;
        String[] currentToken;

        currentLine = sc.nextLine();
        currentToken = currentLine.split(DELIMITER);

        mode = currentToken[1];

        orderNumber = Integer.parseInt(currentToken[0]);
        System.out.println("");

    }

    public void writeMaxOrderNumber() throws FileNotFoundException {
        Set<Integer> orderNumberSet = orderMap.keySet();
       // if ("production".equals(mode)) {
            maxNumber = 0;
            for (Integer orderNumber : orderNumberSet) {
                if (maxNumber < orderNumber) {
                    maxNumber = orderNumber;
                }
            }

       // }else{
       //     maxNumber=orderNumber;
       // }

        PrintWriter writer = new PrintWriter(new File("config.txt"));

        writer.println(maxNumber + DELIMITER + mode);

        writer.flush();
        writer.close();
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

}
