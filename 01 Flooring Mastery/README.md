**Requirements**  
The application will have a configuration file to set the mode to either Test or Prod. If the mode  
is Test, then the application should not save any order data - order data should only be stored in  
memory. If the mode is prod, then the application should read and write order information from  
a file called Orders_MMDDYYYY.txt  
An Order consists of an order number, customer name, state, tax rate, product type, area, cost  
per square foot, labor cost per square foot, material cost, labor cost, tax, and total.  
Taxes and Product Type information can be found in the Data/Taxes.txt and Data/Products.txt  
files. The customer state and product type entered by a user must match items in the data.  
This information is read in from the file in both production and test mode.  
Orders_06012013.txt is a sample row of data for one order.  
For the UI, it should create a menu to prompt the user for what they would like to do:  

    *****************************************************************************  
     **Flooring Program**  

    1. Display Orders  
    2. Add an Order 
    3.  Edit an Order  
    4. Remove an Order
    5. Save Current Work
    6. Quit

    *****************************************************************************

This Project rather uses very basic concept of Java to perform the CRUD functionality. Flooring master doesn't   
uses some of the core concept of Object Oriented Programming including Inheritance and Polymorphism.    
Projects following the mastery project will include all these concepts.   

100% of the DAO is covered by unit testing. There are couple of tests which had to be grey box testing rather    
than black box due to the nature of the method. I've separated the main project from test so that the unit test   
won't override the data every time test is performed.   