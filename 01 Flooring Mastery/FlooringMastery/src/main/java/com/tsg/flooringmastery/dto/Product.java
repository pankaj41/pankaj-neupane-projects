/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dto;

/**
 *
 * @author apprentice
 */
public class Product {
   
    private String productdType;
    private double costPSF;
    private double laborPSF;

    

    public String getProductdType() {
        return productdType;
    }

    public void setProductdType(String productdType) {
        this.productdType = productdType;
    }

    public double getCostPSF() {
        return costPSF;
    }

    public void setCostPSF(double costPSF) {
        this.costPSF = costPSF;
    }

    public double getLaborPSF() {
        return laborPSF;
    }

    public void setLaborPSF(double laborPSF) {
        this.laborPSF = laborPSF;
    }
    
}
