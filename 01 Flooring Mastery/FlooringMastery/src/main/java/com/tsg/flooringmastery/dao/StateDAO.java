/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.State;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class StateDAO {
    String DELIMITER = ",";
    ArrayList<State> stateTaxArrayList = new ArrayList<>();
    
    public void loadStateTax() throws FileNotFoundException{
        Scanner sc = new Scanner(new BufferedReader(new FileReader("StateTaxRate.txt")));
        String currentLine;
        String[] currentTokens;
        
        while (sc.hasNext()) {
            currentLine = sc.nextLine();
            currentTokens = currentLine.split(DELIMITER);
            
            State state = new State();
            state.setState(currentTokens[0]);
            state.setTaxRate(Double.parseDouble(currentTokens[1]));
            
            stateTaxArrayList.add(state);
        }
        sc.close();
    }
    
    public ArrayList<String> stateName(){
        ArrayList<String> stateNames = new ArrayList<>();
        for (State state : stateTaxArrayList) {
            stateNames.add(state.getState());            
        }
        return stateNames;
    }
    public double getStateTax(String state){
        double stateTaxRate=0;
        for (State state1 : stateTaxArrayList) {
            if (state1.getState().equalsIgnoreCase(state)) {
                stateTaxRate = state1.getTaxRate();
            }
            
        }
        return stateTaxRate;
    }
    
}
