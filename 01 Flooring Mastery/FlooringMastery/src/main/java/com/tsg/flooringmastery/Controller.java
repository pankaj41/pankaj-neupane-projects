/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery;

import com.tsg.flooringmastery.dao.OrderDAO;
import com.tsg.flooringmastery.dao.ProductDAO;
import com.tsg.flooringmastery.dao.StateDAO;
import com.tsg.flooringmastery.dto.Order;
import com.tsg.flooringmastery.dto.Product;
import com.tsg.flooringmastery.ui.ConsoleIO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class Controller {
    
    StateDAO stateDao = new StateDAO();
    ProductDAO productDao = new ProductDAO();
    OrderDAO orderDao = new OrderDAO();
    ConsoleIO con = new ConsoleIO();
    
    public void run() {
        try {
            orderDao.loadTodaysFile();
            loadFilesWhenProgramStarts();
            // orderDao.loadConfigFile();
            //orderDao.loadFile("03082016");
            boolean loop = true;
            while (loop) {
                
                printMenu();
                int userSelection = con.readInt("Please select option from above menu:\n", 1, 6);
                switch (userSelection) {
                    case 1:
                        displayOrders();
                        //con.print("Displaying orders....");
                        break;
                    case 2:
                        addOrder();
                        //con.print("\nOrder Added.\n");
                        break;
                    case 3:
                        update();
                        //con.print("\nOrder Edited.\n");
                        break;
                    case 4:
                        removeOrder();
                        //con.print("\nOrder Removed.\n");
                        break;
                    case 5:
                        orderDao.writeFileForAllDatesInHashMap();
                        con.print("\nSession Saved.");
                        //con.print("\nMain Menu\n");
                        loop = false;
                        break;
                    case 6:
                        String userSave1 = con.readString("\nWould you like to save? Y/N \n");
                        String userSave = userSave1.toUpperCase();
                        if (userSave.equalsIgnoreCase("y") || userSave.equalsIgnoreCase("Yes")) {
                            orderDao.writeFileForAllDatesInHashMap();
                            con.print("\nSession Saved.\n");
                            con.print("\nThank You.");
                        } else {
                            con.print("\nSession not Saved.\n");
                            con.print("\nThank You.");
                        }
                        loop = false;
                        break;
                }
            }
        } catch (FileNotFoundException e) {
            con.print("File not found.. ");
        } catch (IOException e) {
            con.print("File not created.. ");
        }
        
    }
    
    public void printMenu() {
        con.print("***********************************************************\n"
                + "\n"
                + "\t\tWelcome to Flooring Bros. LLC"
                + "\n"
                + "\n"
                + "\n1. Display orders\n"
                + "\n"
                + "2. Add an Order\n"
                + "\n"
                + "3. Edit an Order\n"
                + "\n"
                + "4. Remove an Order\n"
                + "\n"
                + "5. Save current Work\n"
                + "\n"
                + "6. Quit\n"
                + "\n");
        con.print("***********************************************************\n");
        
        productDao.getProductTypeList();
        stateDao.stateName();
    }
    
    public void loadFilesWhenProgramStarts() throws FileNotFoundException, IOException {
        stateDao.loadStateTax();
        productDao.loadProductInfo();
        orderDao.loadTodaysFile();
        orderDao.loadConfigFile();
    }

    //TODO take this method out before comitting
    private void addOrder() throws FileNotFoundException, IOException {
        String customerName1 = con.readString("\nCustomer Name: \n");
        String customerName = Character.toUpperCase(customerName1.charAt(0)) + customerName1.substring(1);
        //reads StatetaxDAO 
        con.print("");
        ArrayList<String> stateNamesList = stateDao.stateName();
        for (String string : stateNamesList) {
            con.printSameLn(string + "\t");
        }
        con.print("");
        
        String userState1 = con.readStringFromArrayList(stateNamesList, "Please enter state from above:\n");
        String userState = userState1.toUpperCase();
        con.print("");
        double taxRate = stateDao.getStateTax(userState);
        con.print("State tax for " + userState + " is " + "%" + taxRate);
        con.print("");

        //reads products from product list
        ArrayList<String> productList = productDao.getProductTypeList();
        for (String string : productList) {
            con.printSameLn(string + "\t");
        }
        con.print("");
        String userSelectProduct1 = con.readStringFromArrayList(productList, "Please select product from above:\n");
        String userSelectProduct = Character.toUpperCase(userSelectProduct1.charAt(0)) + userSelectProduct1.substring(1);
        Product product = productDao.getproduct(userSelectProduct);
        double costPerSQFt = product.getCostPSF();
        double laborCostPerSqFt = product.getLaborPSF();
        //con.print(userSelectProduct);
        con.print("\n" + userSelectProduct + " cost per sq ft: $" + costPerSQFt);
        con.print(userSelectProduct + " labor cost per sq ft: $" + laborCostPerSqFt);
        
        con.print("");
        DecimalFormat df = new DecimalFormat("#.##");
        double area = con.readDouble("Enter Area: ");
        String materialCostString = df.format(area * costPerSQFt);
        double materialCost = Double.parseDouble(materialCostString);
        String laborCostString = df.format(area * laborCostPerSqFt);
        double laborCost = Double.parseDouble(laborCostString);
        String taxString = df.format((materialCost + laborCost) * taxRate * 0.01);
        double tax = Double.parseDouble(taxString);
        String t = df.format(materialCost + laborCost + tax);
        double total = Double.parseDouble(t);
        
        con.print("");
        con.print("\nOrder Summary");
        con.print("----------------------------------");
        con.print("Customer Name: \t\t" + customerName);
        con.print("State:\t\t\t" + userState);
        con.print("State Tax: \t\t%" + taxRate);
        con.print("Product: \t\t" + userSelectProduct);
        con.print("Cost PSF: \t\t$" + costPerSQFt);
        con.print("Labor cost PSF:\t\t$" + laborCostPerSqFt);
        con.print("Area: \t\t\t" + area);
        con.print("Material cost: \t\t$" + materialCost);
        con.print("Labor cost: \t\t$" + laborCost);
        con.print("Tax \t\t\t$" + tax);
        con.print("Total: \t\t\t$" + total);
        
        String userCommit = con.readString("\nDo you want to commit to this Order? Y/N\n");
        if (userCommit.equalsIgnoreCase("y") || userCommit.equalsIgnoreCase("Yes")) {
            Order order = new Order();
            order.setCustomerName(customerName);
            order.setState(userState);
            order.setTaxRate(taxRate);
            order.setProductType(userSelectProduct);
            order.setArea(area);
            order.setCostPerSqft(costPerSQFt);
            order.setLaborCostPerSqFt(laborCostPerSqFt);
            order.setMaterialCost(materialCost);
            order.setLaborCost(laborCost);
            order.setTax(tax);
            order.setTotal(total);
            
            orderDao.addOrder(order);
            con.print("\nOrder Added.\n");
            con.print("\nMain Menu\n");
        } else {
            con.print("\nOrder not Added.\n");
            con.print("\nMain Menu\n");
        }
        
    }
    
    private void displayOrders() throws FileNotFoundException {
        String date = con.readString("\nPlease enter the date of order mmddyyyy:\n");
        String fileName = "Orders_" + date + ".txt";
        File file = new File(fileName);
        if (file.exists()) {
            orderDao.loadFileForGivenDate(date);
            printOrdersForGivenDate(date);
            int orderNumber = con.readInt("\nEnter the orderNumber\n");
            
            Order order = orderDao.getOrderByOrderNumber(orderNumber);
            if (order == null) {
                con.print(orderNumber + " doesn't exist for " + date);
            } else {
                con.print("");
                con.print("\nOrder Summary");
                con.print("----------------------------------");
                con.print("Date: \t\t\t" + order.getDate());
                con.print("Order Number: \t\t" + order.getOrderNumber() + "");
                con.print("Customer Name: \t\t" + order.getCustomerName());
                con.print("State: \t\t\t" + order.getState());
                con.print("Tax Rate: \t\t%" + order.getTaxRate());
                con.print("Product: \t\t" + order.getProductType());
                con.print("Cost PSF: \t\t$" + order.getCostPerSqft());
                con.print("Labor PSF: \t\t$" + order.getLaborCostPerSqFt());
                con.print("Area: \t\t\t" + order.getArea());
                con.print("Material cost:\t\t$" + order.getMaterialCost());
                con.print("Labor Cost: \t\t$" + order.getLaborCost());
                con.print("Tax: \t\t\t$" + order.getTax());
                con.print("Total Cost: \t\t$" + order.getTotal());
                con.print("");
                con.print("");
                con.print("\nMain Menu\n");
            }
        } else {
            con.print("\nNo record exist for " + date);
            con.print("\nMain Menu\n");
        }
    }
    
    private void removeOrder() throws FileNotFoundException {
        String date = con.readString("\nPlease enter the date of order mmddyyyy:\n");
        String fileName = "Orders_" + date + ".txt";
        File file = new File(fileName);
        if (file.exists()) {
            orderDao.loadFileForGivenDate(date);
            printOrdersForGivenDate(date);
            int orderNumber = con.readInt("\nEnter the Order Number:\n");
            
            Order order = orderDao.getOrderByOrderNumber(orderNumber);
            if (order == null) {
                con.print("\n" + orderNumber + " doesn't exist for " + date + "\n");
            } else {
                con.print("");
                con.print("\nOrder Summary");
                con.print("----------------------------------");
                con.print("Date: \t\t\t" + order.getDate());
                con.print("Order Number: \t\t" + order.getOrderNumber() + "");
                con.print("Customer Name: \t\t" + order.getCustomerName());
                con.print("State: \t\t\t" + order.getState());
                con.print("Tax Rate: \t\t%" + order.getTaxRate());
                con.print("Product: \t\t" + order.getProductType());
                con.print("Cost PSF: \t\t$" + order.getCostPerSqft());
                con.print("Labor PSF: \t\t$" + order.getLaborCostPerSqFt());
                con.print("Area: \t\t\t" + order.getArea());
                con.print("Material cost:\t\t$" + order.getMaterialCost());
                con.print("Labor Cost: \t\t$" + order.getLaborCost());
                con.print("Tax: \t\t\t$" + order.getTax());
                con.print("Total Cost: \t\t$" + order.getTotal());
                con.print("");
                
                String comfirmDeleteOrder = con.readString("\nDo you want to delete the above Order? Y/N\n");
                if (comfirmDeleteOrder.equalsIgnoreCase("y") || comfirmDeleteOrder.equalsIgnoreCase("Yes")) {
                    orderDao.removeOrder(order.getOrderNumber());
                    con.print("\nOrder Removed.\n");
                    con.print("\nMain Menu\n");
                } else {
                    con.print("\nOrder not deleted.\n");
                    con.print("\nMain Menu\n");
                }
            }
        } else {
            con.print("\nNo record exist for " + date + "\n");
            con.print("\nMain Menu\n");
        }
    }
    
    private void update() throws FileNotFoundException {
        String date = con.readString("\nPlease enter the date of order mmddyyyy:\n");
        String fileName = "Orders_" + date + ".txt";
        File file = new File(fileName);
        if (file.exists()) {
            orderDao.loadFileForGivenDate(date);
            printOrdersForGivenDate(date);
            int orderNumber = con.readInt("\nEnter the order number:\n");
            Order order = orderDao.getOrderByOrderNumber(orderNumber);
            if (order == null) {
                con.print("\nOrder Number " + orderNumber + " does not exist.\n");
                con.print("\nMain Menu\n");
            } else {
                con.print("\nProgram in Edit Mode....\n");
                con.print("-----------------------------------");

                /*con.print("Date :" + order.getDate());
                con.print("Order Number: " + order.getOrderNumber() + "");

                con.print("Customer Name: " + order.getCustomerName());*/
                //changing customer name
                String cName = order.getCustomerName();
                String customerName = con.readString("\nEnter customer name(" + order.getCustomerName() + "):\n");
                //String customerName = Character.toUpperCase(customerName1.charAt(0)) + customerName1.substring(1);

                if (customerName.equalsIgnoreCase("")) {
                    customerName = cName;
                }

                //changing state
                con.print("");
                ArrayList<String> stateNamesList = stateDao.stateName();
                for (String string : stateNamesList) {
                    con.printSameLn(string + "\t");
                }
                con.print("");
                stateNamesList.add("");
                String state = order.getState();//state of the object currently
                String userState1 = con.readStringFromArrayList(stateNamesList, "Please enter state from above(" + order.getState() + "):\n");
                String userState = userState1.toUpperCase();
                
                if (userState.equalsIgnoreCase("")) {
                    userState = state;
                }
                double taxRate = stateDao.getStateTax(userState);//getting the tax rate
                con.print("\nState tax for " + userState + " is " + "%" + taxRate);
                con.print("");

                //changing products
                con.print("");
                ArrayList<String> productList = productDao.getProductTypeList();
                for (String string : productList) {
                    con.printSameLn(string + "\t");
                }
                con.print("");
                productList.add("");
                String product1 = order.getProductType();
                String userSelectProduct = con.readStringFromArrayList(productList, "Please select product from above(" + order.getProductType() + "):\n ");
                //String userSelectProduct = Character.toUpperCase(userSelectProduct1.charAt(0)) + userSelectProduct1.substring(1);
                if (userSelectProduct.equals("")) {
                    userSelectProduct = product1;
                }
                //getting costPSF and Labor cost per sq ft 
                Product product = productDao.getproduct(userSelectProduct);
                double costPerSQFt = product.getCostPSF();
                double laborCostPerSqFt = product.getLaborPSF();
                //con.print(userSelectProduct);
                con.print("\n" + userSelectProduct + " cost per sq ft: $" + costPerSQFt);
                con.print(userSelectProduct + " labor cost per sq ft: $" + laborCostPerSqFt);
                DecimalFormat df = new DecimalFormat("#.##");

                //changing Area
                double area1 = order.getArea();
                String areaString = con.readString("\nEnter Area(" + order.getArea() + "):\n");
                double area = 0;
                
                try {
                    area = Double.parseDouble(areaString);
                    
                } catch (NumberFormatException numberFormatException) {
                }
                if (areaString.equals("")) {
                    area = area1;
                }
                
                String materialCostString = df.format(area * costPerSQFt);
                double materialCost = Double.parseDouble(materialCostString);
                String laborCostString = df.format(area * laborCostPerSqFt);
                double laborCost = Double.parseDouble(laborCostString);
                String taxString = df.format((materialCost + laborCost) * taxRate * 0.01);
                double tax = Double.parseDouble(taxString);
                String t = df.format(materialCost + laborCost + tax);
                double total = Double.parseDouble(t);
                
                con.print("");
                con.print("\nOrder Summary");
                con.print("----------------------------------");
                //con.print(order.getOrderNumber());
                con.print(order.getDate());
                con.print("Customer Name: \t\t" + customerName);
                con.print("State:\t\t\t" + userState);
                con.print("State Tax: \t\t%" + taxRate);
                con.print("Product: \t\t" + userSelectProduct);
                con.print("Cost PSF: \t\t$" + costPerSQFt);
                con.print("Labor cost PSF:\t\t$" + laborCostPerSqFt);
                con.print("Area: \t\t\t" + area);
                con.print("Material cost: \t\t$" + materialCost);
                con.print("Labor cost: \t\t$" + laborCost);
                con.print("Tax \t\t\t$" + tax);
                con.print("Total: \t\t\t$" + total);
                String confirmEditOrder1 = con.readString("\nDo you want to edit the above Order? Y/N\n");
                String confirmEditOrder = confirmEditOrder1.toUpperCase();
                if (confirmEditOrder.equalsIgnoreCase("y") || confirmEditOrder.equalsIgnoreCase("Yes")) {
                    order.setCustomerName(customerName);
                    order.setState(userState);
                    order.setTaxRate(taxRate);
                    order.setProductType(userSelectProduct);
                    order.setArea(area);
                    order.setCostPerSqft(costPerSQFt);
                    order.setMaterialCost(materialCost);
                    order.setLaborCost(laborCost);
                    order.setTax(tax);
                    order.setTotal(total);
                    orderDao.updateOrder(order, order.getOrderNumber());
                    con.print("\nOrder Edited.\n");
                    con.print("\nMain Menu\n");
                } else {
                    con.print("\nOrder not edited.\n");
                    con.print("\nMain Menu\n");
                }
            }
        } else {
            con.print("\nNo record exists for " + date + "specified\n");
            con.print("\nMain Menu\n");
        }
    }
    
    public void printOrdersForGivenDate(String date) {
        ArrayList<Order> ordersForGivenDate = orderDao.getOrdertsForGivenDate(date);
        con.print("These are the orders currently in the record for " + date + "\n");
        con.print("Order No\tCustomer \tState\tProduct\t\tTotal");
        for (int i = 0; i < ordersForGivenDate.size(); i++) {
            con.print(ordersForGivenDate.get(i).getOrderNumber() + "\t\t" + ordersForGivenDate.get(i).getCustomerName() + "\t\t"
                    + ordersForGivenDate.get(i).getState() + "\t" + ordersForGivenDate.get(i).getProductType() + "\t\t$" + ordersForGivenDate.get(i).getTotal());
        }
        /*for (Order order : ordersForGivenDate) {
            con.print("Order Number: \t\t"+order.getOrderNumber());
            con.print("Date of purchase: \t"+ order.getDate());
            con.print("Customer Name: \t\t"+order.getCustomerName());
            con.print("Product sold: \t\t"+order.getProductType());
            con.print("Total: \t\t\t"+order.getTotal());
            con.print("");
        }*/
        
    }
    
}
